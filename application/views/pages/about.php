
			<div id="content">
					
				<div class="post fullpost">
											
					<div class="post-img">
					
						<img width="844" height="400" src="images/17-844x400.jpg" alt="17">										
					
					</div><!-- END POST-IMG -->		
						
					<div class="post-entry">
					
						<h1>About Us</h1>
						
						<p>
							WordPress users may install and switch between themes. Themes allow users to change the look and functionality of a WordPress 
							website or installation without altering the information content or structure of the site. Themes may be installed using the 
							WordPress “Appearance” administration tool or theme folders may be uploaded via FTP. The PHP,HTML (HyperText Markup Language) 
							and CSS (Cascading Style Sheets) code found in themes can be added to or edited for providing advanced features. Thousands of 
							WordPress themes exist, some free, and some paid for templates. WordPress users may also create and develop their own custom 
							themes if they have the knowledge and skill to do so.
						</p>
						
						<p>
							WordPress’s plugin architecture allows users to extend its features. WordPress has over 30,000 plugins available, each of 
							which offers custom functions and features enabling users to tailor their sites to their specific needs. These customizations 
							range from search engine optimization, to client portals used to display private information to logged in users, to content 
							displaying features, such as the addition of widgets and navigation bars.
						</p>
						
						<p>
							Native applications exist for WebOS, Android, iOS (iPhone, iPod Touch, iPad), Windows Phone, and BlackBerry. These applications, 
							designed by Automattic, allow a limited set of options, which include adding new blog posts and pages, commenting, moderating 
							comments, replying to comments in addition to the ability to view the stats.
						</p>
						
						<p>
							WordPress also features integrated link management; a search engine–friendly, clean permalink structure; the ability to assign 
							multiple categories to articles; and support for taggingof posts and articles. Automatic filters are also included, providing 
							standardized formatting and styling of text in articles (for example, converting regular quotes to smart quotes). WordPress also 
							supports the Trackback and Pingback standards for displaying links to other sites that have themselves linked to a post or an article.
						</p>							
													
					</div><!-- END POST-ENTRY -->						
					
				</div><!-- END POST -->								
			
			</div><!-- END CONTENT -->
				
						
			<div id="sidebar">
		
				<div class="widget">
				
					<h4 class="block-heading"><span>Pages</span></h4>

					<ul>
						
						<li class="page_item"><a href="about.html">About Us</a></li>
						
						<li class="page_item"><a href="allposts.html">All Posts</a></li>
						
						<li class="page_item"><a href="contact.html">Contact Us</a></li>
						
						<li class="page_item"><a href="fullwidth.html">FullWidth Page</a></li>
						
						<li class="page_item"><a href="shortcodes.html">Shortcodes</a></li>
					
					</ul>
					
				</div><!-- END WIDGET -->
		
				<div class="widget">
				
					<h4 class="block-heading"><span>About Us</span></h4>			
					
					<div class="about">
					
						<div class="about-img">
						
								<img src="images/14-172x110.jpg" alt="About Us">
								
						</div><!-- END ABOUT-IMG -->						
						
						<div class="about-text">
						
							<strong>Founder</strong> and CEO of The <em>Energy Project</em>, a company that helps individuals and organizations fuel energy, engagement, focus and productivity by harnessing the science of high performance.					
						
						</div><!-- END ABOUT-TEXT -->						
						
						<div class="about-more">						
							
							<a href="about.html" title="Read More" class="btn btn-medium btn-red">Read More</a>
							
						</div><!-- END ABOUT MORE -->						
					
					</div><!-- END ABOUT -->
					
				</div><!-- END WIDGET -->
				
			</div><!-- END SIDEBAR -->	