<div class="divider-block"></div>

			<div id="content">
					
				<div class="post fullpost animate" data-animate="fadeInDown">
				
											
					<div class="post-heading">
						
						<h2>Contact Us</h2>
						
					</div><!-- END POST HEADING -->	
											
					
					<div class="post-entry">
					
						<p><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d93563.11459335827!2d74.59128869999999!3d42.8760664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x389eb7dc91b3c881%3A0x492ebaf57cdee27d!2z0JHQuNGI0LrQtdC6LCDQmtC40YDQs9C40LfQuNGP!5e0!3m2!1sru!2s!4v1419248321613" width="800" height="600"></iframe></p>
					
						<address>
							
							<strong>Contacts</strong><br>
							
							1600 Pennsylvania Ave NW, Washington, DC 20500<br>
							
							+990 (312) 123 45 67<br>
							
							<span><a style="color:#ee4400;" title="hello@dopamine.com" href="#">hello@dopamine.com</a></span><br>
						
						</address>
						
						<p>
							
							<strong>Business Hours:</strong><br>
							
							Monday – Friday: 08:00 – 17:59<br>
							
							Saturday – Sunday: 09:00 – 13:59
						
						</p>
						
						<p style="padding-bottom: 30px; ">
							
							<strong style="margin-bottom: 5px;">Social Networks</strong><br>
						
							<a href="#" title="facebook" style="color:#d5d5d5; font-size: 20px; margin-right:20px; font-weight: 300;"><i class="fa fa-facebook"></i></a>
						
							<a href="#" title="twitter" style="color:#d5d5d5; font-size: 20px; margin-right:20px; font-weight: 300;"><i class="fa fa-twitter"></i></a>
						
							<a href="#" title="instagram" style="color:#d5d5d5; font-size: 20px; font-weight: 300;"><i class="fa fa-instagram"></i></a>
						
						</p>
						
						<h3>Contact Form</h3>
						
						<div class="wpcf7" id="wpcf7-f853-p402-o1" lang="en-US" dir="ltr">
							
							<div class="screen-reader-response"></div>
						
							<form name="" action="http://designread.co/html/path/contact.html" method="post" class="wpcf7-form" novalidate="novalidate">
								
								<div style="display: none;">
									
									<input type="hidden" name="_wpcf7" value="853">
									
									<input type="hidden" name="_wpcf7_version" value="4.1">
									
									<input type="hidden" name="_wpcf7_locale" value="en_US">
									
									<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f853-p402-o1">
									
									<input type="hidden" name="_wpnonce" value="48276ea358">
								
								</div>
								
								<p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name"></span></p>
								
								<p> <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email"></span></p>
								
								<p> <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span></p>
								
								<p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit"><img class="ajax-loader" src="../../path/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;"></p>
								
								<div class="wpcf7-response-output wpcf7-display-none"></div>
							
							</form><!-- END FORM -->
						
						</div><!-- END CONTACT FORM7 -->						
													
					</div><!-- END POST-ENTRY -->				
					
				</div><!-- END POST -->							
			
			</div><!-- END CONTENT -->
				
						
			<div id="sidebar">
		
				<div class="widget animate" data-animate="fadeInUp">
				
					<h4 class="block-heading"><span>Pages</span></h4>

					<ul>
						
						<li class="page_item"><a href="about.html">About Us</a></li>
						
						<li class="page_item"><a href="allposts.html">All Posts</a></li>
						
						<li class="page_item"><a href="contact.html">Contact Us</a></li>
						
						<li class="page_item"><a href="fullwidth.html">FullWidth Page</a></li>
						
						<li class="page_item"><a href="shortcodes.html">Shortcodes</a></li>
					
					</ul>
					
				</div><!-- END WIDGET -->
		
				<div class="widget animate" data-animate="fadeInLeft">
				
					<h4 class="block-heading"><span>About Us</span></h4>			
					
					<div class="about">
					
						<div class="about-img">
						
								<img src="images/14-172x110.jpg" alt="About Us">
								
						</div><!-- END ABOUT-IMG -->						
						
						<div class="about-text">
						
							<strong>Founder</strong> and CEO of The <em>Energy Project</em>, a company that helps individuals and organizations fuel energy, engagement, focus and productivity by harnessing the science of high performance.					
						
						</div><!-- END ABOUT-TEXT -->						
						
						<div class="about-more">						
							
							<a href="about.html" title="Read More" class="btn btn-medium btn-red">Read More</a>
							
						</div><!-- END ABOUT MORE -->						
					
					</div><!-- END ABOUT -->
					
				</div><!-- END WIDGET -->
				
			</div><!-- END SIDEBAR -->