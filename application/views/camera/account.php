
<?php if ($this->aauth->is_loggedin()) { 
//    
//    $gender = $this->aauth->get_user()->gender_id;
//    $this->db()->r
    
    ?>

    <div class="hero-unit padding20">
        <h4><i class="icon-cog"></i>&nbsp;USERNAME ACCOUNT SETTING</h4>
        <hr/>
        <h1><?= $this->aauth->get_user()->fullname;?><small>'s Profile</small></h1>
        <hr/>
        <h4><strong><i class="icon-envelope"></i></strong> <?= $this->aauth->get_user()->email;?></h4>
        <h4><strong><i class="icon-tag"></i></strong>&nbsp; <?= $this->aauth->get_user()->phone;?></h4>
    </div>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="row">
                <h3></h3>
            </div>
        </div>
    </div>
    <div class="container-fluid padding10">
        <div class="row-fluid padding20">
            <div class="fg-gray row bg-black">
                <h3>UPDATE PERSONAL DETAILS</h3>
                <hr/>
            </div>
            <div class="span7">
                <h4><i class="icon-edit"></i>&nbsp;EDIT INFO</h4>

                <div class="hero-unit place_center">
                    <div class="row">

                    </div>
                    <form class="form-inline">

                        <div class="row">
                            <input class="span10"required  type="text" disabled name="fullname" placeHolder="<?= $this->aauth->get_user()->fullname?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" title="<?= $this->aauth->get_user()->fullname?> you can't Edit field">
                                                 
                        </div>
                        <div class="row">

                            <input class="span5" required type="text" name="email" placeHolder="<?= $this->aauth->get_user()->email?>">
                            <input class="span5" type="text" required name="phone" placeHolder="<?= $this->aauth->get_user()->phone?>">                        
                        </div>
                        <hr/>
                        <div class="row">
                            <input  class="span5" type="text" required id="statofO" placeHolder="State of Origin">
                            <input class="span5" type="text" required id="nationality" placeHolder="Nationality">
                        </div>
                        <hr/>
                        <div class="row">
                            <input type="password" required name="password" class="span10 btn-circle" placeHolder="Enter Password to update">
                        </div>
                        <hr/>
                        <div class="row">
                            <button class="btn btn-success">UPDATE</button>
                            <button class="btn btn-warning">CANCEL</button>
                        </div>


                    </form>

                </div>
            </div>
            <div class="span4 pull-right">
                <div class="span7">
                    <img  alt="" src="<?= base_url('assets/camtales/img/picture5.png') ?>">
                <div class="row place_center">
                    <button class="btn btn-inverse">Change Profile Pic</button>
                </div>

            </div>
            <div class="navbar-fixed-bottom navbar-static">
                <button class="btn btn-large"><i class="icon-question-sign"></i>&nbsp;Report Bug</button>
            </div>
        </div>
    </div>
</div>
</div>
<?php }else{
    header('location:'.base_url('#login'));
    
}
?>