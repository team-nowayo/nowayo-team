<div id="wrapper">
				
		<div class="container">
		
			<div class="divider-block"></div>

			<div class="post fullpost errorpage">
			
				<div class="post-entry">
				
					<h1><?= $this->title?></h1>
					
					<p>
					
						The requested URL  
						
						 was not found on this server.					
					</p>
					
					<span class="error-text">That is all we know.</span>
					
					<a class="btn btn-medium btn-red" href="<?=base_url();?>"><i class="fa fa-home"></i>BACK TO HOMEPAGE</a><!-- button -->										
				
				</div><!-- END POST-ENTRY -->	
			
			</div><!-- END POST -->	
		
		</div><!-- END CONTAINER -->		
			
	</div><!-- END WRAPPER -->
	