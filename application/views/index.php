<div id="wrapper">	
	
		<div class="container">		
			
			<div class="featured">					
				
				<div class="flexslider post-img">				
							
					<div class="flex-viewport">
					
						<ul class="slides">

						<?php foreach ($news as $data) {?>
							
							<li>	
								
								<img width="400" height="160" src="<?php echo base_url('uploads').'/'. $data['link'];?>" alt="Image">					
								
								<div class="featured-block">															
									
									<div class="featured-inner">
										
										<div class="post-heading">
											<h1><?php echo $data['news_title'];?></h1>
										
										</div>
										
										<div class="meta">
											
											<span class="date"><?php echo date('F jS, Y h:i:s A',$data['news_added']);?></span>	
										
										</div>	
										<a class="btn btn-medium btn-red" href="<?= base_url('news/views/'.$data['news_id'].'/' . $data['news_title_url']);?>" title="<?= $data['news_title'];?>">READ MORE</a>
									
									</div>
								
								</div>
							
							</li>	

							<?php } ?>					
										
												
						</ul>
						
					</div>
					
				</div><!-- END FLEXSLIDER -->								
									
			</div><!-- END FEATURED -->	
			
							
			<div id="content">			
					<?php foreach ($news as $data) {?>
				<div class="post list">				
					
					<div class="post-entry">				
										
						
						<h4><a href="<?= base_url('news/views/'.$data['news_id'].'/' . $data['news_title_url']);?>" title="<?= $data['news_title'];?>"><?php echo $data['news_title'];?></a></h4>					
						
						<p><?php // echo strtok($data['news_content'], '\n');?></p>	
					<!--	<span class="meta">								

							<span class="share"><a href="#"><i class="fa fa-share"></i> 1</a></span>
								
							<span class="comment"><a href="#"><i class="fa fa-comment-o"></i> 2</a></span>
							
							<span class="eye"><a href="#"><i class="fa fa-eye"></i> 778</a></span>																	
															
						</span><!-- END META -->								
												
					</div><!-- END POST-ENTRY -->						
					
				</div><!-- END POST LIST -->	
				
				<?php } ?>
				
			
				
			</div><!-- END CONTENT -->

			
			<div id="sidebar">
			
			<div class="widget">
												
				<div class="widget">
				
					<h4 class="block-heading"><span>Photostream</span></h4>
					
					<div class="twitter_feed">

					<ul class="fetched_tweets dark">
									<?php foreach ($limit as $images) { ?>
								<li class="tweets_avatar">
											
									<img src="<?php echo base_url('uploads/').'/'.$images['link'];?>" height="50" width="50" alt="Image-<?php echo base_url('uploads/').'/'.$images['upload_id'];?>"/>		        	
								</li>
									 <?php }?> 
								
					</ul>
						 <a href="<?php echo base_url('cam-tales');?>" title="View All" class="btn btn-medium btn-red">View All</a>

												
					</div>
				</div>
				</div>
					<div class="widget">		
				
					<h4 class="block-heading"><span>Recent Posts</span></h4>		
					
					<ul>
							<?php foreach ($list as $key => $value) {?>
								
						<li><a href="<?=base_url('news/views/'.$value['news_id'].'/' . $value['news_title_url']);?>"><?php echo $value['news_title'];?></a><span class="post-date"><?php echo date('F jS, Y',$value['news_added']);?></span></li>
							<?php }?>
					</ul>
					
				</div>
				</div>
			</div>
		</div>
<div class="pagination">
		<?php echo $this->pagination->create_links();?>
</div>