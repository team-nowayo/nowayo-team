<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contest extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('upload');
		$this->load->model(array('upload_model','state_model','gender_model','contest_model','vote_model'));
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$data['users'] = $this->contest_model->getActiveUser();
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/camera/list',$data);
		$this->load->view('cpanel/layout/footer');
	}

	public function addContest()
	{
		if($this->input->post()){
		$data['status'] = '1';
		$data['theme'] = $this->input->post('fashion');
		$data['start_date'] = $this->input->post('start_date');
		$data['end_date'] = $this->input->post('end_date');
		$check = $this->contest_model->create_contest($data);
		if(check){
			$this->session->set_flashdata('success', 'Contest Has Been Created');
			}
			return FALSE;
		}
		return FALSE;
	}

	public function addUserToContest()
	{
		$data['contest_id'] = $contest_id = 1;
		$data['contestant_id'] = $contestant_id = $this->uri->segment(4);

		if($this->contest_model->checkContestant($contest_id, $contestant_id)){
			$this->session->set_flashdata('msg', 'User Has Already Been Added Earlier');
			redirect(base_url('cp/camera'),'refresh');
		}
		else{
			$check = $this->contest_model->addUserToContest($data);
			if($check){
				$this->session->set_flashdata('success', 'User Has Been Added');
				redirect(base_url('cp/camera'),'refresh');
			}
			$this->session->set_flashdata('msg', 'An Error Occured! Could not Add User');
			redirect(base_url('cp/camera'),'refresh');
		}
	}

	public function removeUser($image_id=array())
	{
		# code...
	}
	public function voteUser()
	{
		$data['contest_id'] = '1';
		$data['contestant_id'] = '2';
		$data['voter_id'] = $this->session->userdata('id');
		$check = $this->vote_model->addVote($data);
	}
}

/* End of file Contest.php */
/* Location: ./application/controllers/cpanel/Contest.php */