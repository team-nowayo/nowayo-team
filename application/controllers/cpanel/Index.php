<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Index extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('Aauth');
		$this->load->library('user_agent');
		$this->load->model(array('gender_model','state_model','settings_model','news_model','upload_model'));
		$this->session->set_userdata('redirect_back', $this->agent->referrer());  
		if(!$this->aauth->is_member('Administrator')){
			redirect(base_url());
		}
	}
	private function redirect_url()
	{
	if( $this->session->userdata('redirect_back') ) {
   	$redirect_url = $this->session->userdata('redirect_back');  // grab value and put into a temp variable so we unset the session value
   	$this->session->unset_userdata('redirect_back');
   	redirect( $redirect_url );
   		}
	}

	public function index()
	{
		if (!$this->aauth->is_loggedin()){
		$this->title = "Nowayo CPanel";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/login');
		$this->load->view('cpanel/layout/footer');
		}
		else{
			redirect(base_url('cp/dashboard'),'refresh');
		}
	}

	public function home()
	{
		if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cp/'));
		}
		else{
		$data['users'] = $this->aauth->list_users();
		$data['list'] = $this->news_model->countAll();
		$data['upload'] = $this->upload_model->fetchByType('cam-tales');
		$this->title = "WebAdmin Dashboard";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/layout/sidebar',$data);
		$this->load->view('cpanel/dashboard',$data);
		$this->load->view('cpanel/layout/footer');
		}
	}
 
	public function users()
	{	
		if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cp/'));
		}
		else{
		$data['users'] = $this->aauth->list_users();
		$data['roles'] = $this->aauth->list_groups();
		$this->title = "Nowayo User Base";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/users/list',$data);
		$this->load->view('cpanel/layout/footer');
		}
	}
	public function moderators()
	{	
		if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cp/'));
		}
		else{
		$data['mods'] = $this->aauth->list_users('1');
		$this->title = "Nowayo Moderators";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/mods/list',$data);
		$this->load->view('cpanel/layout/footer');
		}
	}
	public function presenters()
	{	
		if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cp/'));
		}
		else{
		$data['presenters'] = $this->aauth->list_users('2');
		$this->title = "Nowayo Presenters";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/presenters/list',$data);
		$this->load->view('cpanel/layout/footer');
		}
	}
	public function groups()
	{	
		if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cp/'));
		}
		else{
		$data['groups'] = $this->aauth->list_groups();
		$this->title = "Nowayo RBAC";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/groups/list',$data);
		$this->load->view('cpanel/layout/footer');
		}
	}
	public function ban()
	{	
		if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cp/'));
		}
		else{
		$data['groups'] = $this->aauth->list_groups();
		$data['ban'] = $this->aauth->list_ban();
		$this->title = "Banned Users";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/users/banned',$data);
		$this->load->view('cpanel/layout/footer');
		}
	}
	public function settings()
	{	
		if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cp/'));
		}
		else{
		$data['settings'] = $this->settings_model->getVar();
		$this->title = "Website Configuration";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/settings',$data);
		$this->load->view('cpanel/layout/footer');
		}
	}
	public function login()
	{

		$this->form_validation->set_rules('email', 'Email Address', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		$email 		= html_escape($this->input->post('email'));
		$password 	= html_escape($this->input->post('password'));


		if($this->form_validation->run() == true){
			$check = $this->aauth->login($email, $password , $remember = FALSE, $totp_code = NULL);
			//var_export($check);
			if($check)
	{
		$this->session->set_flashdata('success',"You Have Logged In Successfully.");
		redirect(base_url('cp/dashboard'));
	}
		else
	{
		$this->session->set_flashdata('msg',"An Error Has Occurred. Please Check Your Login Credentials");
		redirect(base_url('cp/'));
	}
		}
		else
		{
			$this->session->set_flashdata('msg', "We Cannot log you in at this moment. Please try again Later");
			redirect(base_url('cp/'));
		}
	}
	public function create_user()
	{	
		if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cp/'));
		}
		else{
		$data['roles'] = $this->aauth->list_groups();
		$data['gender'] = $this->gender_model->loadAll();
		$data['state'] = $this->state_model->loadAll();
		$this->title = "Add New User";
		$this->load->view('cpanel/layout/header');
		$this->load->view('cpanel/users/add',$data);
		$this->load->view('cpanel/layout/footer');
		}
	}
	public function register()
	{
		$email 		= html_escape($this->input->post('email'));
		$password 	= html_escape($this->input->post('password'));
		$username 	= html_escape($this->input->post('user_name'));
		$fullname 	= html_escape($this->input->post('full_name'));
		$residence 	= html_escape($this->input->post('residence'));
		//$country 	= html_escape($this->input->post('country'));
		$phone 		= html_escape($this->input->post('phone'));
		$gender 	= html_escape($this->input->post('gender'));
		//$picture 	= "";
		$check 		= $this->aauth->create_user($email, $password, $username ,$fullname, $residence, $phone, $gender);

		if($check)
	{
		$this->session->set_flashdata('success', "Your Account Has Been Created Successfully");
		$this->redirect_url();
	}
		else
	{
		$this->session->set_flashdata('msg', "Failed To Create Account At This Moment. Please Try Again Later");
      	$this->redirect_url();
		}
	}
	public function is_loggedin() {

        if ($this->aauth->is_loggedin()){
            return true;
        }
        else
        {
        	return false;
        }
    }
    public function logout() {

      if($this->aauth->logout()){
      	redirect(base_url(),'refresh');
       	}
    }

    function ban_user($value) {

        $a = $this->aauth->ban_user($value);

        if($a){
        $this->session->set_flashdata('success', "User #".$value."Has Been banned");
		$this->redirect_url();
        }
        else{
        $this->session->set_flashdata('msg', "Banning Process Has Failed... Please Try Again Later");
		$this->redirect_url();
        }
    }
    function unban_user($value) {

        $a = $this->aauth->unban_user($value);
		if($a){
        $this->session->set_flashdata('success', "User #".$value."Has Been unbanned");
		$this->redirect_url();
        }
        else{
        $this->session->set_flashdata('msg', "Unbanning Process Has Failed... Please Try Again Later");
		$this->redirect_url();
        }
    }
    public function is_banned($value) {
        return $this->aauth->is_banned($value);
    }
    public function news()
    {	$data['list'] = $this->news_model->loadView();
    	$this->title = "Nowayo News Panel";
    	$this->load->view('cpanel/layout/header');
    	$this->load->view('cpanel/posts/list',$data);
    	$this->load->view('cpanel/layout/footer');
    }

    public function assign_role($value)
    {	
    	$data['mods'] = $this->aauth->list_groups();
    	$data['assign'] = $this->aauth->get_user($value);
    	$this->title = "Assign Role To User";
    	$this->load->view('cpanel/layout/header');
    	$this->load->view('cpanel/groups/assign',$data);
    	$this->load->view('cpanel/layout/footer');
    }
    public function create_post()
    {	

    	if (!$this->aauth->is_loggedin()) {
				redirect(base_url());
		}
			else
			{
    	$data['list'] = $this->news_model->loadView();
    	$data['state'] = $this->state_model->loadAll();
    	$this->title = "Nowayo News Panel";
    	$this->load->view('cpanel/layout/header');
    	$this->load->view('cpanel/posts/add',$data);
    	$this->load->view('cpanel/layout/footer');
    	}
    }
    private function set_upload_options()
        {   

            $config = array();
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['overwrite']     = FALSE;
            return $config;
        }

        private function url_pl($text)
	{
		$litery1 = array('ę', 'ó', 'ą', 'ś', 'ł', 'ż', 'ź', 'ć', 'ń', 'Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń', ' ');		
		$litery2 = array('e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n', 'e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n', '-');
		
		$text = trim($text);
		$text = str_replace($litery1, $litery2, $text);
		$text = strtolower($text);
		
		$pattern = '/[^a-zA-Z0-9\-^]/';
		$text = preg_replace($pattern, '', $text);
		
		$pattern = '/-+/';
		$text = preg_replace($pattern, '-', $text);
		
		return $text;
	}

	
    
}