<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cam extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('Aauth','upload'));
		$this->load->model(array('upload_model','state_model','gender_model','upload_model'));
		$this->load->helper(array('form', 'url', 'date'));

		
	}
	public function index()
	{
		$this->all();
    }
	public function upload(){

	if (!$this->aauth->is_loggedin()) {
				redirect(base_url('cam-tales'));
		}
	else{
		$this->title = "Upload To Camera Tales";
		$this->load->view('camera/layout/header');
		$this->load->view('camera/upload');
		$this->load->view('camera/layout/footer');
		}
	}

	function do_upload()
	{
		if($_POST){
				$description = $this->input->post('description');

				$this->load->library('upload');
				$files = $_FILES;

				 $count = count($_FILES['userfile']['name']);
			
				for($i=0; $i<$count; $i++)
            {
            	$filename = preg_replace('/\s+/', '', $files['userfile']['name']);
                $_FILES['userfile']['name']= 'cam'.-time().$filename[$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];    


               $configure =  $this->upload->initialize($this->set_upload_options());
               	$implode = $_FILES['userfile']['name'];
               //var_dump($implode);
                if($this->upload->do_upload() == False)
                {
                	$this->session->set_flashdata('msg', 'An Error Occurred. Please Try Again Later');
					redirect('cam-tales/create');
                }
                else
                {
            $data['description'] = $description;
			$data['link'] = $implode;
			$data['user_id'] = $this->session->userdata('id');
			$data['type'] = 'cam-tales';
			$data['server_created'] = time();

			//echo $implode;
			$this->upload_model->insert($data);
				
			//if($this->upload_model->insert($data)){
				//var_dump($this->upload_model->insert($data));
			//$this->session->set_flashdata('success', 'Your File Has been Uploaded Successfully');
			//redirect('cam-tales/');
                //}

            }

		}
		 redirect('cam-tales/');
	}else{
		$this->session->set_flashdata('msg', 'No File(s) Was Uploaded');
			redirect('cam/create');
	}
	}

	private function set_upload_options()
        {   

            $config = array();
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']  = '5024';
            $config['overwrite']     = FALSE;
            return $config;
        }

	private function all() {
		
		$data['list'] = $this->upload_model->get_list();
		$data['uploads'] = $this->upload_model->loadView();
		$data['upload'] = $this->upload_model->fetchByUser($this->session->userdata('id'));
		$data['presenter'] = $this->upload_model->fetch_presenters();

		$data['state']= $this->state_model->loadAll();
		$data['gender']= $this->gender_model->loadAll();
		$data['limit'] = $this->upload_model->Limit();
		


		$this->load->library('pagination');

		$config['base_url'] = base_url('cam-tales/');
		$config['total_rows'] = $this->upload_model->countAll();
		$config['per_page'] = "10";

		$data['pagination'] = $this->pagination->initialize($config);
		$this->title = "Our Camera Tales";
		$this->load->view('camera/layout/header');
		$this->load->view('camera/index', $data);
		$this->load->view('camera/layout/footer');

	}

	public function views($user_id, $date = null)
	{
		if( ctype_alpha($date)){
			$this->session->set_flashdata('msg', 'Invalid Date Format');
				}
		if(empty($date)) {
		$data['upload'] = $this->upload_model->fetchByUser($user_id);
	}
	else{
		$data['upload'] = $this->upload_model->fetchByUserDate($user_id, $date);
	}
		$this->title = $this->aauth->get_user($user_id)->fullname . ' Album';
		$this->load->view('camera/layout/header');
		$this->load->view('camera/view',$data);
		$this->load->view('camera/layout/footer');
	}

	public function presenters()
	{	$data['presenters'] = $this->upload_model->fetch_presenters();
		$this->load->view('camera/layout/header');
		$this->load->view('camera/presenters', $data);
		$this->load->view('camera/layout/footer');
	}

	public function delete($upload_id)
	{
		if($_POST){
			//$type = 'cam-tales';
			$check = $this->upload_model->delete($upload_id);
			if($check){
			$this->session->set_flashdata('success', 'Image Has Been Deleted Successfully');
			redirect(base_url('cam/views/'.$this->session->userdata('id')),'refresh');
			}
			else{
				$this->session->flashdata('msg', 'Image Could Not Be Deleted At The Moment. Please Try Again Later');
				redirect(base_url('cam/views/'.$this->session->userdata('id')),'refresh');
			}
		}
		else{
			$this->session->set_flashdata('msg', 'No Image Was Selected');
			redirect(base_url('cam/views/'.$this->session->userdata('id')),'refresh');
		}
	}

	public function account()
	{
		$this->load->view('camera/layout/header');
		$this->load->view('camera/account');
		$this->load->view('camera/layout/footer');
	}

	public function preview()
	{
		$this->load->view('camera/layout/header');
		$this->load->view('camera/preview-account');
		$this->load->view('camera/layout/footer');
	}

	public function contest()
	{
		$data['contest'] = $this->upload_model->loadView();
		$this->load->view('camera/layout/header');
		$this->load->view('camera/contest', $data);
		$this->load->view('camera/layout/footer');
	}

	public function event()
	{
		$this->load->view('camera/layout/header');
		$this->load->view('camera/event');
		$this->load->view('camera/layout/footer');
	}
}
?>
