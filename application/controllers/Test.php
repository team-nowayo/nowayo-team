<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('activity_model');
		$this->load->library(array('Images'));
	}

	public function index()
	{
		print_r($this->activity_model->getMax());
	}

}

/* End of file Test.php */
/* Location: ./application/controllers/Test.php */