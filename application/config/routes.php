<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['cam-tales'] = 'cam/index';
$route['cam-tales/create'] = 'cam/upload';
$route['cam-tales/activity'] = 'cam/index';
$route['cam-tales/uploads'] = 'cam/index';
$route['cam-tales/import'] = 'cam/index';
$route['cam-tales/account'] = 'cam/account';
$route['cam-tales/presenters'] = 'cam/presenters';
$route['cam-tales/events'] = 'cam/event';
$route['cam-tales/views/(:num)'] = 'cam/view/$1';
$route['(:any)'] = 'cam/view/$1';
//End Of Camera Pages
$route['account/reset/(:num)'] = 'index/reset/$1';
$route['account'] = 'index/user';

$route['contact'] = 'page/contact';
$route['about'] = 'page/about';

//News Pages
$route['news'] = 'news/index';
$route['news/create'] = 'cpanel/index/create_post';
//End Of News Pages
//User Pages
$route['auth/edit-password'] = 'home/edit_password';
$route['auth/edit-account'] = 'home/edit_account';
$route['default_controller'] = 'index';
$route['404_override'] = 'Error';
$route['translate_uri_dashes'] = FALSE;

$route['cp'] = 'cpanel/index/index';
$route['cp/dashboard'] = 'cpanel/index/home';
$route['cp/users'] = 'cpanel/index/users';
$route['cp/users/create'] = 'cpanel/index/create_user';
$route['cp/moderators'] = 'cpanel/index/moderators';
$route['cp/presenters'] = 'cpanel/index/presenters';
$route['cp/groups'] = 'cpanel/index/groups';
$route['cp/settings'] = 'cpanel/index/settings';
$route['cp/ban-user'] = 'cpanel/index/ban';
$route['cp/news'] = 'cpanel/index/news';
$route['cp/register'] = 'cpanel/index/create_user';
$route['cp/create-post'] = 'cpanel/index/create_post';
$route['cp/create-news'] = 'cpanel/index/create_post';
$route['cp/modify-post/(:any)'] = 'news/modify/$1';

$route['cp/contest/open'] = 'cpanel/contest/addcontest';
$route['cp/contest/join/(:any)'] = 'cpanel/contest/addUserToContest/$1';
$route['cp/contest/vote'] = 'cpanel/contest/voteUser';

$route['cam-tales/contest'] = 'cam/contest';


$route['admin/'] = 'administrative/index';
$route['admin/roles'] = 'administrative/roles';
$route['admin/users'] = 'administrative/users';

$route['contact'] = 'pages/contact';

$route['comments'] = 'comments/index';
$route['menu'] = 'menu/index';

//blog pages
$route['blog'] = 'blog/index';
$route['blog/create'] = 'blog/create_news';
$route['blog/modify'] = 'blog/modify';
//end of blog pages

//Camera Management 
$route['cp/camera'] = 'cpanel/camera/index';
//End Of Camera Management

$route['test'] = 'test/index';