<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['images']['table'] = 'nowayo_uploads';

$config['images']['album'] = 'nowayo_album';

$config['images']['image_to_vote'] = 'nowayo_contest';

$config['images']['vote_image'] = 'nowayo_vote';

$config['images']['db_profile'] = 'default';