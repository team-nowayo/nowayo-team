<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contest_model extends CI_Model {

		public $table_name = 'nowayo_contests';

		function __construct(){
			parent::__construct();
		}

		public function create_contest($data = array())
		{
			if (empty($data)) {
				return FALSE;	
			}
			$insert = $this->db->insert('nowayo_contests', $data);
			if($insert){
				return TRUE;
			}
			return FALSE;
		}

		function getUsers(){
			$this->db->select('*');
			$this->db->from($this->table_name);
			return $this->db->get()->result_array();
		}

		function getContestants($contest_id){
			$query = $this->db->select('contestant_id')->from('nowayo_contests_contestants')->WHERE('contest_id', $contest_id);
			return $this->db->get()->result_array();
		}

		function checkContestant($contest_id, $contestant_id)
		{
			$contestants_arr = $this->getContestants($contest_id);
			foreach ($contestants_arr as $key => $value)
			{
				if(in_array($contestant_id, $value)){ 
					return true;
				} else {continue;}
			}
			return false;
		}

		public function getActiveUser()
	{
		$this->db->select('user_id, COUNT(*) as total');
		$this->db->group_by('user_id');
		$this->db->limit(20);
		$result = $this->db->get('nowayo_events_log');
		$result_array = $result->result_array(); 
		return $result_array;
	}

	public function addUserToContest($data = array())
	{
		if (empty($data)) {
				return FALSE;	
			}
			$insert = $this->db->insert('nowayo_contests_contestants', $data);
			if($insert){
				return TRUE;
			}
			return FALSE;
	}

	public function removeUserFromContest($id)
	{
		$delete = $this->db->query('DELETE FROM nowayo_contest WHERE user_id ='.$id);
		if($delete){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}



}

/* End of file Contest_model.php */
/* Location: ./application/models/Contest_model.php */