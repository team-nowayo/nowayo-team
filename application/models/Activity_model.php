<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getMax()
	{
		$this->db->select('user_id, COUNT(*) as total');
		$this->db->group_by('user_id');
		$this->db->limit(20);
		$result = $this->db->get('nowayo_events_log');
		$result_array = $result->result_array();  //Get the result as array
		return $result_array;
	}
}
/* End of file Activity_model.php */
/* Location: ./application/models/Activity_model.php */
